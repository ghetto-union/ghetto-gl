#ifndef GHETTO_GL_SHADER_HPP
#define GHETTO_GL_SHADER_HPP
#include <iostream>
#include "gl_util.hpp"

#define ASSIGN_UNIFORM_LOCATION(name) assignUniformLocation(#name, name);
#define DERIVE_FROM_SHADER(CHILD, BASE, ...)      \
  struct CHILD : public BASE {                    \
    GLuint __VA_ARGS__;                           \
    void load(std::string const &vertex_path,     \
              std::string const &fragment_path) { \
      BASE::load(vertex_path, fragment_path);     \
      ASSIGN_UNIFORM_LOCATION(__VA_ARGS__)        \
    }                                             \
  };
namespace ghetto {
namespace gl {
struct Shader {
  GLuint program;
  GLuint clip_matrix;
  GLuint screen_dimension;
  GLuint timestamp;
  Shader() : program(0){};
  void assignUniformLocation(std::string uniform_name,
                             GLuint &uniform_location) {
    uniform_location = glGetUniformLocation(program, uniform_name.c_str());
    if (uniform_location == -1) {
      std::cerr << "Uniform '" << uniform_name << "' not found in program."
                << std::endl;
    }
  }
  template <typename T, typename... Args>
  void assignUniformLocation(std::string uniform_name, T &first,
                             Args &... args) {
    std::size_t pos = uniform_name.find(", ");
    assignUniformLocation(uniform_name.substr(0, pos), first);
    uniform_name.erase(0, pos + 2);
    assignUniformLocation(uniform_name, args...);
  }
  void load(std::string const &vertex_path, std::string const &fragment_path) {
    std::cout << "Wiring " << vertex_path << " and " << fragment_path
              << std::endl;
    loadProgram(program, vertex_path, fragment_path);
    ASSIGN_UNIFORM_LOCATION(clip_matrix);
    ASSIGN_UNIFORM_LOCATION(screen_dimension);
    ASSIGN_UNIFORM_LOCATION(timestamp);
  }
};
}  // namespace gl
}  // namespace ghetto
#endif
