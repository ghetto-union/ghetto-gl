#ifndef GHETTO_GUI_VIEW_POLE_HPP
#define GHETTO_GUI_VIEW_POLE_HPP

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
namespace ghetto {
namespace gl {
struct ViewData {
  glm::vec3 target_x;
  glm::fquat orientation;
  float radius;
  float spin;
};

struct ViewScale {
  float min_radius;
  float max_radius;
  float coarse_radius_step;
  float fine_radius_step;
  float coarse_position_step;
  float fine_position_step;
  float coarse_translation_scale;
  float fine_translation_scale;
  float rotation_step;
};

class ViewPole {
 public:
  ViewPole(ViewData const &, ViewScale const &, int = GLFW_MOUSE_BUTTON_LEFT,
           int = GLFW_MOUSE_BUTTON_MIDDLE);
  glm::fquat calculateRotation() const;
  glm::mat4 calculateMatrix() const;
  void registerMouseButton(int, int, int, glm::vec2 const &);
  void registerMouseMove(glm::vec2 const &);
  void registerMouseScroll(float, int);

 private:
  enum RotateMode { DUAL_AXIS, BIAXIAL, SPIN };

  void startDragRotate(glm::vec2 const &, RotateMode);
  void continueDragRotate(glm::vec2 const &);
  void terminateDragRotate(glm::vec2 const &);

  void startDragTranslate(glm::vec2 const &);
  void continueDragTranslate(glm::vec2 const &);
  void terminateDragTranslate(glm::vec2 const &);

  void changeOrientation(glm::vec2 const &);
  void changeSpin(glm::vec2 const &);
  void changeRadius(float);
  void changeTarget(glm::vec2 const &);

  ViewData view_;
  ViewData initial_view_;

  ViewScale view_scale_;
  int rotation_button_;
  int translation_button_;

  bool is_dragging_;
  int dragging_button;
  RotateMode rotate_mode_;
  glm::vec2 drag_start_x_;
  ViewData drag_start_view_;
};
}  // namespace gl
}  // namespace ghetto
#endif
