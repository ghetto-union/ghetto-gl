#ifndef GHETTO_GL_UTIL_HPP
#define GHETTO_GL_UTIL_HPP
#include <glad/glad.h>

#include <string>
#include <vector>

namespace ghetto {
namespace gl {
GLuint loadShader(GLenum, std::string const &);
GLuint createProgram(std::vector<GLuint> const &);
GLuint loadProgram(std::string const &, std::string const &);
void loadProgram(GLuint &, std::string const &, std::string const &);
void createTexture(GLuint *, int, int, GLenum = GL_RGBA, GLint = GL_RGBA32F);
void createFramebufferWithTexture(GLuint *, GLuint *, int, int, GLuint = 0,
                                  GLenum = GL_RGBA, GLint = GL_RGBA32F);
void resizeTexture(GLuint, int, int, GLenum = GL_RGBA, GLint = GL_RGBA32F);

template <class T>
void allocateGraphicsArray(GLuint *vbo, std::size_t element_count,
                           T const *src_ptr = nullptr,
                           GLenum buffer_type = GL_ARRAY_BUFFER,
                           GLenum usage = GL_DYNAMIC_DRAW) {
  glGenBuffers(1, vbo);
  glBindBuffer(buffer_type, *vbo);
  glBufferData(buffer_type, element_count * sizeof(T), src_ptr, usage);
  glBindBuffer(buffer_type, 0);
}

void freeGraphicsArray(GLuint *vbo);

template <class ClientT>
void copyGraphicsArrayClientToServer(GLuint dst_vbo, ClientT const *src_ptr,
                                     std::size_t buffer_start_element_offset,
                                     std::size_t element_count) {
  glBindBuffer(GL_ARRAY_BUFFER, dst_vbo);
  glBufferSubData(GL_ARRAY_BUFFER,
                  buffer_start_element_offset * sizeof(ClientT),
                  element_count * sizeof(ClientT),
                  reinterpret_cast<const GLvoid *>(src_ptr));
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}
}  // namespace gl
}  // namespace ghetto
#endif
