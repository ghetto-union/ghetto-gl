#include "ghetto/gl_util.hpp"
#include <algorithm>
#include <exception>
#include <fstream>
#include <ghetto/file.hpp>
#include <iostream>

namespace ghetto {
namespace gl {
GLuint loadShader(GLenum shader_type, std::string const& shader_path) {
  GLuint shader = glCreateShader(shader_type);

  std::string shader_code = ghetto::readFile<char>(shader_path);
  char const* shader_code_c_str = shader_code.c_str();
  glShaderSource(shader, 1, &shader_code_c_str, NULL);
  glCompileShader(shader);

  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE) {
    GLint info_log_length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
    GLchar* info_log = new GLchar[info_log_length + 1];
    glGetShaderInfoLog(shader, info_log_length, NULL, info_log);
    std::cerr << "Problem loading shader: " << shader_path << std::endl;
    std::cerr << info_log << std::endl;
    delete[] info_log;
  }
  return shader;
}

GLuint createProgram(std::vector<GLuint> const& shaders) {
  GLuint program = glCreateProgram();
  for (GLuint shader : shaders) {
    glAttachShader(program, shader);
  }
  glLinkProgram(program);

  GLint status;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  if (status == GL_FALSE) {
    GLint info_log_length;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
    GLchar* info_log = new GLchar[info_log_length + 1];
    glGetProgramInfoLog(program, info_log_length, NULL, info_log);
    std::cerr << info_log << std::endl;
    delete[] info_log;
  }
  return program;
}

GLuint loadProgram(std::string const& vertex_path,
                   std::string const& fragment_path) {
  std::vector<GLuint> shaders;
  shaders.push_back(loadShader(GL_VERTEX_SHADER, vertex_path));
  shaders.push_back(loadShader(GL_FRAGMENT_SHADER, fragment_path));
  GLuint program = createProgram(shaders);
  std::for_each(shaders.begin(), shaders.end(), glDeleteShader);
  return program;
}

void loadProgram(GLuint& program, std::string const& vertex_path,
                 std::string const& fragment_path) {
  // NOTE: even when the new program cannot be compiled, the old program can
  // still run because it is only marked as delete but not immediately deleted
  if (program != 0) glDeleteProgram(program);
  program = loadProgram(vertex_path, fragment_path);
}

void createTexture(GLuint* texture, int w, int h, GLenum format,
                   GLint internal_format) {
  glGenTextures(1, texture);
  glBindTexture(GL_TEXTURE_2D, *texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  glTexImage2D(GL_TEXTURE_2D, 0, internal_format, w, h, 0, format, GL_FLOAT,
               nullptr);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void createFramebufferWithTexture(GLuint* texture, GLuint* fbo, int w, int h,
                                  GLuint depth_attachment, GLenum format,
                                  GLint internal_format) {
  glGenFramebuffers(1, fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, *fbo);
  gl::createTexture(texture, w, h, format, internal_format);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         *texture, 0);
  if (depth_attachment != 0) {
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, depth_attachment);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void resizeTexture(GLuint texture, int w, int h, GLenum format,
                   GLint internal_format) {
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, internal_format, w, h, 0, format, GL_FLOAT,
               nullptr);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void freeGraphicsArray(GLuint* vbo) {
  if (*vbo == 0) return;
  glDeleteBuffers(1, vbo);
  *vbo = 0;
}
}  // namespace gl
}  // namespace ghetto
