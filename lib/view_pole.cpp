#include "ghetto/view_pole.hpp"
#include <glm/gtc/matrix_transform.hpp>
namespace ghetto {
namespace gl {
ViewPole::ViewPole(ViewData const &initial_view, ViewScale const &view_scale,
                   int rotation_button, int translation_button)
    : view_(initial_view),
      initial_view_(initial_view),
      view_scale_(view_scale),
      rotation_button_(rotation_button),
      translation_button_(translation_button),
      is_dragging_(false),
      dragging_button(-1) {}

glm::fquat ViewPole::calculateRotation() const {
  return glm::angleAxis(view_.spin, glm::vec3(0.0f, 0.0f, 1.0f)) *
         view_.orientation;
}

glm::mat4 ViewPole::calculateMatrix() const {
  glm::mat4 result(1.0f);
  result = glm::translate(result, glm::vec3(0.0f, 0.0f, -view_.radius));
  result = result * glm::mat4_cast(calculateRotation());
  result = glm::translate(result, -view_.target_x);
  return result;
}

void ViewPole::registerMouseButton(int button, int action, int modifiers,
                                   glm::vec2 const &x) {
  if (button == rotation_button_) {
    if (action == GLFW_PRESS) {
      if (!is_dragging_) {
        if (modifiers & GLFW_MOD_CONTROL)
          startDragRotate(x, ViewPole::BIAXIAL);
        else if (modifiers & GLFW_MOD_ALT)
          startDragRotate(x, ViewPole::SPIN);
        else
          startDragRotate(x, ViewPole::DUAL_AXIS);
      }
    } else {
      if (is_dragging_) {
        terminateDragRotate(x);
      }
    }
  } else if (button == translation_button_) {
    if (action == GLFW_PRESS) {
      if (!is_dragging_) {
        startDragTranslate(x);
      }
    } else {
      if (is_dragging_) {
        terminateDragTranslate(x);
      }
    }
  }
}

void ViewPole::registerMouseMove(glm::vec2 const &x) {
  if (!is_dragging_) return;
  if (dragging_button == rotation_button_)
    continueDragRotate(x);
  else if (dragging_button == translation_button_)
    continueDragTranslate(x);
}

void ViewPole::registerMouseScroll(float offset, int modifiers) {
  changeRadius(-offset * (modifiers & GLFW_MOD_CONTROL
                              ? view_scale_.coarse_radius_step
                              : view_scale_.fine_radius_step));
}

void ViewPole::startDragRotate(glm::vec2 const &x, RotateMode mode) {
  rotate_mode_ = mode;
  drag_start_x_ = x;
  drag_start_view_ = view_;
  is_dragging_ = true;
  dragging_button = rotation_button_;
}

void ViewPole::continueDragRotate(glm::vec2 const &x) {
  glm::vec2 dx = x - drag_start_x_;
  switch (rotate_mode_) {
    case DUAL_AXIS:
      changeOrientation(dx);
      break;
    case BIAXIAL:
      if (abs(dx.x) > abs(dx.y))
        changeOrientation(glm::vec2{dx.x, 0.0f});
      else
        changeOrientation(glm::vec2{0.0f, dx.y});
      break;
    case SPIN:
      changeSpin(dx);
      break;
    default:
      break;
  }
}

void ViewPole::terminateDragRotate(glm::vec2 const &x) {
  continueDragRotate(x);
  is_dragging_ = false;
}

void ViewPole::startDragTranslate(glm::vec2 const &x) {
  drag_start_x_ = x;
  drag_start_view_ = view_;
  is_dragging_ = true;
  dragging_button = translation_button_;
}

void ViewPole::continueDragTranslate(glm::vec2 const &x) {
  glm::vec2 dx = x - drag_start_x_;
  changeTarget(dx);
}

void ViewPole::terminateDragTranslate(glm::vec2 const &x) {
  continueDragTranslate(x);
  is_dragging_ = false;
}

void ViewPole::changeOrientation(glm::vec2 const &dx) {
  glm::vec2 angle_differences = view_scale_.rotation_step * dx;
  view_.orientation =
      glm::angleAxis(angle_differences.y, glm::vec3(1.0f, 0.0f, 0.0f)) *
      drag_start_view_.orientation *
      glm::angleAxis(angle_differences.x, glm::vec3(0.0f, 1.0f, 0.0f));
}

void ViewPole::changeSpin(glm::vec2 const &dx) {
  view_.spin = drag_start_view_.spin + dx.x * view_scale_.rotation_step;
}

void ViewPole::changeRadius(float dr) {
  view_.radius += dr;
  if (view_.radius < view_scale_.min_radius)
    view_.radius = view_scale_.min_radius;
  if (view_.radius > view_scale_.max_radius)
    view_.radius = view_scale_.max_radius;
}

void ViewPole::changeTarget(glm::vec2 const &dx) {
  glm::fquat full_rotation = calculateRotation();
  glm::vec3 target_dx =
      glm::vec3{-dx.x, dx.y, 0.0f} * glm::mat3_cast(full_rotation);
  view_.target_x = drag_start_view_.target_x +
                   view_scale_.fine_translation_scale * target_dx;
}
}  // namespace gl
}  // namespace ghetto
